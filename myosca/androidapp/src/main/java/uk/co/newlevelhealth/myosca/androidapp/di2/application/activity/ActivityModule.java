package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.HomeActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.login.LoginActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.RegisterActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.start.StartActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.welcome.WelcomeActivity;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.IHomeComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.login.ILoginComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register.IRegisterComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.start.IStartComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.welcome.IWelcomeComponent;

@Module(subcomponents = {
        IStartComponent.class,
        IWelcomeComponent.class,
        ILoginComponent.class,
        IRegisterComponent.class,
        IHomeComponent.class})
public abstract class ActivityModule {

    @Binds
    @IntoMap
    @ActivityKey(LoginActivity.class)
    public abstract AndroidInjector.Factory<? extends Activity>
    bindLoginActivityInjectorFactory(ILoginComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(RegisterActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindRegisterActivityInjectorFactory(IRegisterComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(HomeActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindHomeActivityInjectorFactory(IHomeComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(WelcomeActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindWelcomeActivityInjectorFactory(IWelcomeComponent.Builder builder);

    @Binds
    @IntoMap
    @ActivityKey(StartActivity.class)
    abstract AndroidInjector.Factory<? extends Activity>
    bindStartActivityInjectorFactory(IStartComponent.Builder builder);
}