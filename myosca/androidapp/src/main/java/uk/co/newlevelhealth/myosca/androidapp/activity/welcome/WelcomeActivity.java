package uk.co.newlevelhealth.myosca.androidapp.activity.welcome;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import uk.co.newlevelhealth.myosca.androidapp.R;

public class WelcomeActivity extends AppCompatActivity implements IWelcomeContract.View {
    @Inject
    WelcomeController welcomeController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        bindView();
        welcomeController.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        welcomeController.onDestroy();
    }

    @Override
    public void bindView() {
        ButterKnife.bind(this);
    }

    @Override
    public void inject() {
        AndroidInjection.inject(this);
    }
}
