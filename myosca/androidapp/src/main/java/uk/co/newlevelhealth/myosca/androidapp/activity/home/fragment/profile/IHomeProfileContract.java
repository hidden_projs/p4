package uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.profile;

import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IController;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IFragmentView;

public interface IHomeProfileContract {
    interface View extends IFragmentView {

    }

    interface Controller extends IController {

    }
}
