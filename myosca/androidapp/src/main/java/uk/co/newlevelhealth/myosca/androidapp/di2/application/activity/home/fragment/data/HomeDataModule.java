package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.data;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.data.HomeDataFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.data.IHomeDataContract;

@Module
public abstract class HomeDataModule {
    @Binds
    public abstract IHomeDataContract.View bindHomeDataView(HomeDataFragment homeDataFragment);
}
