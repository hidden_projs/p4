package uk.co.newlevelhealth.myosca.androidapp.activity.login;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import uk.co.newlevelhealth.myosca.androidapp.R;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.HomeActivity;

public class LoginActivity extends AppCompatActivity implements ILoginContract.View {
    @Inject
    LoginController loginController;

    @BindView(R.id.login_btn_login)
    AppCompatButton mBtnLogin;

    @BindView(R.id.login_wrapper_un)
    TextInputLayout mWrapperUN;

    @BindView(R.id.login_wrapper_pw)
    TextInputLayout mWrapperPW;

    @BindView(R.id.login_txt_un)
    AppCompatEditText mTxtUN;

    @BindView(R.id.login_txt_pw)
    AppCompatEditText mTxtPW;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bindView();
        loginController.onCreate();
        mWrapperUN.setErrorEnabled(true);
        mWrapperPW.setErrorEnabled(true);
        mTxtUN.setText("admin@as.ru");
        mTxtPW.setText("admin12");
    }

    @OnClick(R.id.login_btn_login)
    void onBtnLoginClick() {
        loginController.attemptLogin();
    }

    @OnClick(R.id.login_btn_skip_login)
    void onBtnSkipLoginClick() {
        Intent intent = new Intent(this, HomeActivity.class);
        this.startActivity(intent);
        this.finish();
    }

    @OnClick(R.id.login_link_register)
    void onLinkRegisterClick() {
        loginController.goToRegister();
    }

    @Override
    public AppCompatButton getMBtnLogin() {
        return mBtnLogin;
    }

    @Override
    public TextInputLayout getMWrapperUN() {
        return mWrapperUN;
    }

    @Override
    public TextInputLayout getMWrapperPW() {
        return mWrapperPW;
    }

    @Override
    public AppCompatEditText getMTxtUN() {
        return mTxtUN;
    }

    @Override
    public AppCompatEditText getMTxtPW() {
        return mTxtPW;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginController.onDestroy();
    }

    @Override
    public void bindView() {
        ButterKnife.bind(this);
    }

    @Override
    public void inject() {
        AndroidInjection.inject(this);
    }
}
