package uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.R;
import uk.co.newlevelhealth.myosca.androidapp.activity.login.LoginActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.RegisterActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details
        .RegisterDetailsFragment;
import uk.co.newlevelhealth.myosca.androidapp.security.AppSecurityManager;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;
import uk.co.newlevelhealth.myosca.androidapp.security.task.RegisterTask;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

public class RegisterEmailController implements IRegisterEmailContract.Controller {
    private final IRegisterEmailContract.View view;

    private final RegisterActivity activity;

    private final Context context;

    private final BroadcastManager broadcastManager;

    private final AuthenticationProvider authenticationProvider;

    private final AppSecurityManager appSecurityManager;

    @Inject
    public RegisterEmailController(@NonNull final IRegisterEmailContract.View view,
                                   @NonNull final RegisterActivity activity,
                                   @NonNull final Context context,
                                   @NonNull final BroadcastManager broadcastManager,
                                   @NonNull final AuthenticationProvider authenticationProvider,
                                   @NonNull final AppSecurityManager appSecurityManager) {
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(authenticationProvider);
        Preconditions.checkNotNull(appSecurityManager);

        this.view = view;
        this.activity = activity;
        this.context = context;
        this.broadcastManager = broadcastManager;
        this.authenticationProvider = authenticationProvider;
        this.appSecurityManager = appSecurityManager;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDestroy() {
        broadcastManager.unregister(this);
    }

    @Override
    public boolean validateUserInput() {
        boolean valid = true;
        String un = view.getMTxtUN().getText().toString().trim();
        String pw = view.getMTxtPWFirst().getText().toString().trim();
        String pw2 = view.getMTxtPWSecond().getText().toString().trim();
        if (un.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(un).matches()) {
            view.getMWrapperUN().setError("Enter a valid email address");
            valid = false;
        } else {
            view.getMWrapperUN().setError(null);
        }
        if (pw.isEmpty() || pw.length() < 4 || pw.length() > 10) {
            view.getMWrapperPWFirst().setError("Between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            view.getMWrapperPWFirst().setError(null);
        }
        if (!(pw.equals(pw2))) {
            view.getMWrapperPWSecond().setError("Passwords do not match");
            valid = false;
        } else {
            view.getMWrapperPWSecond().setError(null);
        }
        return valid;
    }

    @Override
    public void attemptRegister() {
        if (validateUserInput()) {
            User user = new User();
            user.setUsername(view.getMTxtUN().getText().toString().trim());
            user.setPassword(view.getMTxtPWFirst().getText().toString().trim());
            view.getMBtnRegisterEmail().setEnabled(false);
            view.getMBtnRegisterEmail().setAlpha(.5f);
            broadcastManager.register(this);
            new RegisterTask(authenticationProvider, broadcastManager).execute(user);
        }
    }

    @Override
    public void goToLogin() {
        Intent intent = new Intent(context, LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public void showRegisterError(String error) {
        Utils.ToastCreator.showToast(activity, error);
    }

    @Subscribe
    public void onEvent(ApiResponse event) {
        broadcastManager.unregister(this);
        view.getMBtnRegisterEmail().setEnabled(true);
        view.getMBtnRegisterEmail().setAlpha(1f);
        if (event.getStatus() == ApiResponseCode.OK) {
            proceedRegistration(event.getPayload());
        } else {
            showRegisterError(event.getPayload());
        }
    }

    private void proceedRegistration(String token) {
        appSecurityManager.logIn(token);
        final FragmentTransaction transaction =
                activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.register_fragment_container, new RegisterDetailsFragment())
                .commit();
    }

    public void debugSkipRegister() {
        final FragmentTransaction transaction =
                activity.getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.register_fragment_container, new RegisterDetailsFragment())
                .commit();
    }
}
