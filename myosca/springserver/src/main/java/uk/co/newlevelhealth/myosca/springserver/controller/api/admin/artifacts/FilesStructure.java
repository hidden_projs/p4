package uk.co.newlevelhealth.myosca.springserver.controller.api.admin.artifacts;

import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class FilesStructure {

    private static final String APK_NAME = "app-debug-%s.apk";

    private static final String SERVER_NAME = "myoscaserver-0.0.%s.jar";

    private static final String CURRENT_VERSION = "currentversion";

    private static final String CHANGELOG = "changelog";

    @Value("${myosca.artifactsPath}")
    String artifactsPath;

    private int currentVersion;

    private TreeMap<Integer, FileObject> versions;

    private ArrayList<FileObject> versionsList;

    public FilesStructure() {
    }

    public TreeMap<Integer, FileObject> getVersions() {
        return versions;
    }

    public int getCurrentVersion() {
        return currentVersion;
    }

    public ArrayList<FileObject> getVersionsList() {
        return versionsList;
    }

    public void init() {
        versions = new TreeMap<>();
        versionsList = new ArrayList<>();
        try {
            Files.list(Paths.get(artifactsPath)).filter(Files::isDirectory).forEach((Path p) -> {
                try {
                    versions.put(Integer.parseInt(p.getFileName().toString()), new FileObject(
                            Paths.get(p.toString(), String.format(APK_NAME, p.getFileName().toString())).toFile(),
                            Paths.get(p.toString(),
                                    String.format(SERVER_NAME, p.getFileName().toString())
                            ).toFile(),
                            Files.readAllLines(Paths.get(p.toString(), CHANGELOG)),
                            Integer.parseInt(p.getFileName().toString())
                    ));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            currentVersion = Integer.parseInt(Files.readAllLines(Paths.get(artifactsPath, CURRENT_VERSION)).get(0));

            for (Map.Entry<Integer, FileObject> entry : versions.entrySet()) {
                versionsList.add(entry.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
