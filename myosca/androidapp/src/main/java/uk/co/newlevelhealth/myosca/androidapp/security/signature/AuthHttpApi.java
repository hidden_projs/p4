package uk.co.newlevelhealth.myosca.androidapp.security.signature;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;

/**
 * Interface to be used with retrofit for simple rest api calls
 * Specific for External service authentication
 */
public interface AuthHttpApi {

    @POST("/api/auth/login")
    Call<Void> login(@Body RequestBody requestBody);

    @POST("/api/auth/register")
    Call<ApiResponse> register(@Body RequestBody requestBody);
}
