package uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature;

public interface IDaggerInjected {
    void inject();
}
