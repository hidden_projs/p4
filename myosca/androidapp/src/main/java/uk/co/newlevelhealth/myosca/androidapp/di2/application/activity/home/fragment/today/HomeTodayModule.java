package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.today;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.today.HomeTodayFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.today.IHomeTodayContract;

@Module
public abstract class HomeTodayModule {
    @Binds
    public abstract IHomeTodayContract.View bindHomeTodayView(HomeTodayFragment homeTodayFragment);
}
