package uk.co.newlevelhealth.myosca.androidapp.di2.application.module;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import uk.co.newlevelhealth.myosca.androidapp.MyOSCAAndroidApplication;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.MyOSCAAndroidApplicationScope;

import static android.content.Context.MODE_PRIVATE;

@Module
public class ApplicationModule {
    private final MyOSCAAndroidApplication application;

    public ApplicationModule(MyOSCAAndroidApplication application) {
        this.application = application;
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    SharedPreferences provideSharedPreferences() {
        return application.getSharedPreferences(
                application.getPackageName() + "_preferences", MODE_PRIVATE);
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    AccountManager provideAccountManager() {
        return AccountManager.get(application);
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    MyOSCAAndroidApplication provideApplication() {
        return application;
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    Resources provideResources() {
        return application.getResources();
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    Gson provideGSON() {
        return new Gson();
    }
}