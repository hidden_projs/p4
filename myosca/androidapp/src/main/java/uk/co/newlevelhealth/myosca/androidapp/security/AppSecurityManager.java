package uk.co.newlevelhealth.myosca.androidapp.security;

import android.content.SharedPreferences;

import uk.co.newlevelhealth.myosca.androidapp.util.Utils;

public class AppSecurityManager {

    private final SharedPreferences preferences;

    public AppSecurityManager(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    public void logIn(String token) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utils.Constants.LOGIN_STATE, true);
        editor.putString(Utils.Constants.X_AUTH_TOKEN, token);
        editor.apply();
    }

    public void logOut() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utils.Constants.LOGIN_STATE, false);
        editor.putString(Utils.Constants.X_AUTH_TOKEN, null);
        editor.apply();
    }

    public boolean isLoggedIn() {
        return preferences.getBoolean(Utils.Constants.LOGIN_STATE, false);
    }

    public String getToken() {
        return preferences.getString(Utils.Constants.X_AUTH_TOKEN, "");
    }
}
