package uk.co.newlevelhealth.myosca.springserver.controller.api.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseError;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;
import uk.co.newlevelhealth.myosca.springserver.dao.user.MyOSCAUserEntity;
import uk.co.newlevelhealth.myosca.springserver.dao.user.MyOSCAUserRepository;
import uk.co.newlevelhealth.myosca.springserver.settings.security.jwt.TokenAuthenticationService;

@RestController
public class RegisterController {
    @Autowired
    MyOSCAUserRepository myOSCAUserRepository;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    TokenAuthenticationService tokenAuthenticationService;

    @RequestMapping(value = "/api/auth/register", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String doRegister(@RequestBody User user) {
        try {
            String username = user.getUsername();
            String password = user.getPassword();
            MyOSCAUserEntity myOSCAUserEntity = new MyOSCAUserEntity();

            if (myOSCAUserRepository.exists(username))
                return mapper.writeValueAsString(new ApiResponse(ApiResponseCode.ERROR, "User Exists"));

            myOSCAUserEntity.setActive(true);
            myOSCAUserEntity.setEmail(username);
            myOSCAUserEntity.setPassword(password);
            myOSCAUserRepository.save(myOSCAUserEntity);

            String JWT = tokenAuthenticationService.getToken(username);
            return mapper.writeValueAsString(new ApiResponse(ApiResponseCode.OK, JWT));
        } catch (JsonProcessingException e) {
            return String.format(ApiResponseError.VALUE, e.toString());
        }
    }
}
