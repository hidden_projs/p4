package uk.co.newlevelhealth.myosca.androidapp.di2.application;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.google.gson.Gson;

import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import okhttp3.OkHttpClient;
import uk.co.newlevelhealth.myosca.androidapp.MyOSCAAndroidApplication;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.ActivityModule;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.module.ApiModule;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.module.ApplicationModule;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.module.AuthenticationModule;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.module.BackgroundExecutorModule;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.module.BroadcastModule;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.module.HttpModule;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.signature.ApiServiceProvider;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BackgroundExecutor;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;

@MyOSCAAndroidApplicationScope
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ActivityModule.class,
        ApplicationModule.class,
        AuthenticationModule.class,
        BackgroundExecutorModule.class,
        BroadcastModule.class,
        ApiModule.class,
        HttpModule.class})
public interface IApplicationComponent {
    void inject(MyOSCAAndroidApplication myOSCAAndroidApplication);

    MyOSCAAndroidApplication myOscaAndroidApplication();
    AccountManager accountManager();
    Resources resources();
    Context context();
    SharedPreferences sharedPreferences();
    Gson gson();

    OkHttpClient okHttpClient();
    BroadcastManager broadcastManager();
    BackgroundExecutor backgroundExecutor();
    AuthenticationProvider authenticationProvider();
    ApiServiceProvider apiServiceProvider();
}
