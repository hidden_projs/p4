package uk.co.newlevelhealth.myosca.springserver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import uk.co.newlevelhealth.myosca.springserver.controller.api.admin.artifacts.FileObject;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static uk.co.newlevelhealth.myosca.springserver.controller.api.admin.artifacts.AppVersionsController.CURRENT_VERSION;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SystemFileResourcesTest {

    private static final String APK_NAME = "app-debug-%s.apk";

    private static final String SERVER_NAME = "myoscaserver-0.0.%s.jar";

    @Value("${myosca.artifactsPath}")
    String artifactsPath;

    @Test
    public void testArtifactsPathGetAll() {
        assertNotNull(artifactsPath);
        System.out.println("Artifacts Path = " + artifactsPath);
        HashMap<Integer, FileObject> map = new HashMap<>();

        try {
            Files.list(Paths.get(artifactsPath)).filter(Files::isDirectory).forEach((Path p) -> {
                try {
                    map.put(Integer.parseInt(p.getFileName().toString()), new FileObject(
                            Paths.get(p.toString(), String.format(APK_NAME, p.getFileName().toString())).toFile(),
                            Paths.get(p.toString(),
                                    String.format(SERVER_NAME, p.getFileName().toString())
                            ).toFile(),
                            Files.readAllLines(Paths.get(p.toString(), "changelog")),
                            Integer.parseInt(p.getFileName().toString())
                    ));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testArtifactsPathGetCurrentVersion() {
        int version;
        try {
            version = Integer.parseInt(Files.readAllLines(Paths.get(artifactsPath, CURRENT_VERSION)).get(0));
            assertTrue(version > 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
