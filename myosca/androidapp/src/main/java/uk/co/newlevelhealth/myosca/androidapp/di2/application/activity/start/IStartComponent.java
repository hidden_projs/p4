package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.start;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.start.StartActivity;

@Subcomponent(modules = StartModule.class)
public interface IStartComponent extends AndroidInjector<StartActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<StartActivity> {
    }
}
