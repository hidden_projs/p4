package uk.co.newlevelhealth.myosca.springserver.controller.exceptionhandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Controller
public class FilterChainExceptionHandler {
    @RequestMapping(value = "/500")
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    String mapServerException(HttpServletRequest req) {
        String response = "Unknown 500 Error";
        try {
            Throwable ex = (Throwable) req.getAttribute("javax.servlet.error.exception");
            response = new ObjectMapper().writeValueAsString(new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                                                                             "Error 500. " + ex.toString()
            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return response;
    }

    @RequestMapping(value = "/403")
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public @ResponseBody
    String mapUnauthorisedException(HttpServletRequest req) {
        String response = "Unknown 403 Error";
        try {
            Throwable ex = (Throwable) req.getAttribute("javax.servlet.error.exception");
            response = new ObjectMapper().writeValueAsString(new ApiResponse(HttpStatus.FORBIDDEN.value(),
                                                                             "Error 403. " + ex.toString()
            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return response;
    }
}
