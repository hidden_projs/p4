package uk.co.newlevelhealth.myosca.androidapp.security.signature;

import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

public interface AuthenticationProvider {
    ApiResponse login(User user) throws Exception;
    ApiResponse register(User user) throws Exception;
}
