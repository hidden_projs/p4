package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.HomeActivity;

@Subcomponent(modules = {HomeModule.class})
public interface IHomeComponent extends AndroidInjector<HomeActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<HomeActivity> {
    }
}