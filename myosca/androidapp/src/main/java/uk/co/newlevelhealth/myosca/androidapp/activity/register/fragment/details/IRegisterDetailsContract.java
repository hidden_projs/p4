package uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;

import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IController;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IFragmentView;

public interface IRegisterDetailsContract {
    interface View extends IFragmentView {
        AppCompatEditText getmTxtFirstName();
        AppCompatEditText getmTxtLastName();
        AppCompatEditText getmTxtDOB();
        AppCompatEditText getmTxtGender();
        AppCompatEditText getmTxtWeight();
        TextInputLayout getmWrapperFirstName();
        TextInputLayout getmWrapperLastName();
        TextInputLayout getmWrapperDOB();
        TextInputLayout getmWrapperGender();
        TextInputLayout getmWrapperWeight();
        AppCompatButton getmBtnRegisterDetails();
        AppCompatButton getmBtnSkipRegisterDetails();
    }

    interface Controller extends IController {
        boolean validateUserInput();
        void attemptUpdateDetails();
        void showAttemptUpdateError(String error);
        void debugSkipUpdateDetails();
    }
}
