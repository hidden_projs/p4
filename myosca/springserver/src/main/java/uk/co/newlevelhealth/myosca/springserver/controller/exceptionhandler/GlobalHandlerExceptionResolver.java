package uk.co.newlevelhealth.myosca.springserver.controller.exceptionhandler;

import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GlobalHandlerExceptionResolver implements HandlerExceptionResolver, Ordered {
    public int getOrder() {
        return Integer.MIN_VALUE;
    }

    @Override
    public ModelAndView resolveException(HttpServletRequest aReq,
                                         HttpServletResponse aRes,
                                         Object aHandler,
                                         Exception anExc) {
        anExc.printStackTrace(); // again, you can do better than this ;)
        return null; // trigger other HandlerExceptionResolver's
    }
}