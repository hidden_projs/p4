package uk.co.newlevelhealth.myosca.commonobjects.api;

public class ApiResponse {

    private int status;

    private String payload;

    public ApiResponse(int status, String payload) {
        this.status = status;
        this.payload = payload;
    }

    public ApiResponse() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
