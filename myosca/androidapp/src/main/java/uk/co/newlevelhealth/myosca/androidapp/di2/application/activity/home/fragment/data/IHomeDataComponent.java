package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.data;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.data.HomeDataFragment;

@Subcomponent(modules = {HomeDataModule.class})
public interface IHomeDataComponent extends AndroidInjector<HomeDataFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<HomeDataFragment> {
    }
}
