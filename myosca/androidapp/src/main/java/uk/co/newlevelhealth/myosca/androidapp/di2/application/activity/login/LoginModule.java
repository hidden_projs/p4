package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.login;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.login.ILoginContract;
import uk.co.newlevelhealth.myosca.androidapp.activity.login.LoginActivity;

@Module
public abstract class LoginModule {

    @Binds
    public abstract ILoginContract.View bindLoginView(LoginActivity loginActivity);
}
