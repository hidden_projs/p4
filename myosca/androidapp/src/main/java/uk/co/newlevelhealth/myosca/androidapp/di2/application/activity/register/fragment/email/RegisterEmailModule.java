package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register.fragment.email;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email
        .IRegisterEmailContract;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email
        .RegisterEmailFragment;

@Module
public abstract class RegisterEmailModule {
    @Binds
    public abstract IRegisterEmailContract.View bindRegisterEmailView(RegisterEmailFragment
                                                                              fragment);
}
