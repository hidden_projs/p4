package uk.co.newlevelhealth.myosca.androidapp.di2.application.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.MyOSCAAndroidApplicationScope;
import uk.co.newlevelhealth.myosca.androidapp.security.AppSecurityManager;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.ApiServiceProviderImpl;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.signature.ApiServiceProvider;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.UserDetailsApiImpl;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.signatures
        .UserDetailsApi;

@Module
public class ApiModule {

    @Provides
    @MyOSCAAndroidApplicationScope
    public ApiServiceProvider provideApiModule(@NonNull UserDetailsApi userDetailsApi) {
        return new ApiServiceProviderImpl(userDetailsApi);
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    public UserDetailsApi provideUserDetailsApi(final @NonNull OkHttpClient okHttpClient,
                                                final @NonNull Gson gson,
                                                final @NonNull Context context,
                                                final @NonNull AppSecurityManager
                                                        appSecurityManager) {

        return new UserDetailsApiImpl(okHttpClient, gson, context, appSecurityManager);
    }
}
