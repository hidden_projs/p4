package uk.co.newlevelhealth.myosca.androidapp.service.apiservice;

import android.support.annotation.NonNull;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.signature.ApiServiceProvider;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.signatures
        .UserDetailsApi;

public class ApiServiceProviderImpl implements ApiServiceProvider {

    private final UserDetailsApi userDetailsApi;

    public ApiServiceProviderImpl(final @NonNull UserDetailsApi userDetailsApi) {
        Preconditions.checkNotNull(userDetailsApi);
        this.userDetailsApi = userDetailsApi;
    }

    @Override
    public UserDetailsApi userDetailsApi() {
        return this.userDetailsApi;
    }
}
