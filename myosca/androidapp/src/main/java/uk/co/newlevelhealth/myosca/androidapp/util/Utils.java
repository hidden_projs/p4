package uk.co.newlevelhealth.myosca.androidapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.util.Calendar;

import okhttp3.MediaType;

public class Utils {

    private Utils() {
    }

    public static boolean isThisAMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    public static String getTimeOfDay() {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 6 && timeOfDay <= 12) {
            return "morning";
        } else if (timeOfDay > 12 && timeOfDay <= 17) {
            return "afternoon";
        } else if (timeOfDay > 17 && timeOfDay <= 20) {
            return "evening";
        } else {
            return "night";
        }
    }

    public static final class ToastCreator {

        public static void showToast(final @NonNull Context context,
                                     final @NonNull String message) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static final class ConnectionChecker {

        public static boolean hasConnection(final @NonNull Context context) {
            ConnectivityManager connMgr;
            NetworkInfo networkInfo;
            // Check connectivity
            connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
            // Return if no connection
            return !(networkInfo == null || !networkInfo.isConnected());
        }
    }

    public static class Constants {
        public static final String BASE_URL = "http://192.168.1.24:8085/";

        public static final String APPLICATION_JSON = "application/json";

        public static final String MYOSCA_DEBUG_TAG = "MYOSCA_DEBUG_TAG";

        public static final String LOGIN_STATE = "LOGIN_STATE";

        public static final String X_AUTH_TOKEN = "X-AUTH-TOKEN";

        public static final MediaType OK_HTTP3_MEDIA_TYPE_JSON = MediaType.parse(APPLICATION_JSON);

        public static final class HTTPCODES {

            public static final int SC_OK = 200;

            public static final int SC_CREATED = 201;

            public static final int SC_NOT_FOUND = 404;

            public static final int SC_UNAUTHORIZED = 403;

            public static final int SC_BAD_REQUEST = 400;
        }

        public static final class USERCONST {

            public static final String PARAMETER_USERNAME = "username";

            public static final String PARAMETER_PASSWORD = "password";

            public static final String PARAMETER_EMAIL = "email";
        }
    }
}
