package uk.co.newlevelhealth.myosca.androidapp.activity.register;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasDispatchingSupportFragmentInjector;
import uk.co.newlevelhealth.myosca.androidapp.R;

public class RegisterActivity extends AppCompatActivity implements IRegisterContract.View,
        HasDispatchingSupportFragmentInjector {
    @BindView(R.id.register_fragment_container)
    FrameLayout mFragmentContainer;

    @Inject
    DispatchingAndroidInjector<Fragment> supportFragmentInjector;

    @Inject
    RegisterController registerController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        bindView();
        registerController.onCreate();
    }

    @Override
    public void inject() {
        AndroidInjection.inject(this);
    }

    @Override
    public void bindView() {
        ButterKnife.bind(this);
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return supportFragmentInjector;
    }
}
