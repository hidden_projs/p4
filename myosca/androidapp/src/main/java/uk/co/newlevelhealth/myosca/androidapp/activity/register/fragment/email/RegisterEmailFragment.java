package uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import uk.co.newlevelhealth.myosca.androidapp.R;

public class RegisterEmailFragment extends Fragment implements IRegisterEmailContract.View {
    @BindView(R.id.register_email_btn_register)
    AppCompatButton mBtnRegisterEmail;

    @BindView(R.id.register_email_btn_skip_register)
    AppCompatButton mBtnSkipRegisterEmail;

    @BindView(R.id.register_email_wrapper_un)
    TextInputLayout mWrapperUN;

    @BindView(R.id.register_email_wrapper_pw_first)
    TextInputLayout mWrapperPWFirst;

    @BindView(R.id.register_email_wrapper_pw_second)
    TextInputLayout mWrapperPWSecond;

    @BindView(R.id.register_email_txt_un)
    AppCompatEditText mTxtUN;

    @BindView(R.id.register_email_txt_pw_first)
    AppCompatEditText mTxtPWFirst;

    @BindView(R.id.register_email_txt_pw_second)
    AppCompatEditText mTxtPWSecond;

    @Inject
    RegisterEmailController registerEmailController;

    @Override
    public void onAttach(Activity activity) {
        inject();
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_register_fr_email, container, false);
        bindView(view);
        mTxtUN.setText("admin@as.ru");
        mTxtPWFirst.setText("admin12");
        mTxtPWSecond.setText("admin12");
        return view;
    }

    @Override
    public void inject() {
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void bindView(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public AppCompatButton getMBtnRegisterEmail() {
        return mBtnRegisterEmail;
    }

    @Override
    public AppCompatButton getMBtnSkipRegisterEmail() {
        return mBtnSkipRegisterEmail;
    }

    @Override
    public TextInputLayout getMWrapperUN() {
        return mWrapperUN;
    }

    @Override
    public TextInputLayout getMWrapperPWFirst() {
        return mWrapperPWFirst;
    }

    @Override
    public TextInputLayout getMWrapperPWSecond() {
        return mWrapperPWSecond;
    }

    @Override
    public AppCompatEditText getMTxtUN() {
        return mTxtUN;
    }

    @Override
    public AppCompatEditText getMTxtPWFirst() {
        return mTxtPWFirst;
    }

    @Override
    public AppCompatEditText getMTxtPWSecond() {
        return mTxtPWSecond;
    }

    @OnClick(R.id.register_email_btn_register)
    void onBtnRegisterClick() {
        registerEmailController.attemptRegister();
    }

    @OnClick(R.id.register_email_btn_skip_register)
    void onBtnDebugSkipRegisterClick() {
        registerEmailController.debugSkipRegister();
    }

    @OnClick(R.id.register_email_link_login)
    void onLinkRegisterClick() {
        registerEmailController.goToLogin();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        registerEmailController.onDestroy();
    }
}
