package uk.co.newlevelhealth.myosca.androidapp.di2.application;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Annotation to declare that a component will be used during Application runtime
 */
@Scope
@Retention(RetentionPolicy.CLASS)
public @interface MyOSCAAndroidApplicationScope {

}
