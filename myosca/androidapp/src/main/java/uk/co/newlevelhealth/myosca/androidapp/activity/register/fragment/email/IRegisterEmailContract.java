package uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;

import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IController;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IFragmentView;

public interface IRegisterEmailContract {
    interface View extends IFragmentView {
        AppCompatButton getMBtnRegisterEmail();
        AppCompatButton getMBtnSkipRegisterEmail();
        TextInputLayout getMWrapperUN();
        TextInputLayout getMWrapperPWFirst();
        TextInputLayout getMWrapperPWSecond();
        AppCompatEditText getMTxtUN();
        AppCompatEditText getMTxtPWFirst();
        AppCompatEditText getMTxtPWSecond();
    }

    interface Controller extends IController {
        boolean validateUserInput();
        void attemptRegister();
        void goToLogin();
        void showRegisterError(String error);
    }
}
