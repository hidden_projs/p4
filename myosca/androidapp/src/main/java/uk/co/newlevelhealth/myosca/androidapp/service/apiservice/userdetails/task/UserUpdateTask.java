package uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.signature.ApiServiceProvider;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

public class UserUpdateTask extends AsyncTask<User, Void, ApiResponse> {
    private final ApiServiceProvider apiService;

    private final BroadcastManager broadcastManager;

    public UserUpdateTask(@NonNull ApiServiceProvider apiService,
                          @NonNull BroadcastManager broadcastManager) {
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(apiService);
        this.apiService = apiService;
        this.broadcastManager = broadcastManager;
    }

    @Override
    protected ApiResponse doInBackground(User... params) {
        Log.d(Utils.Constants.MYOSCA_DEBUG_TAG, "Started Update User task");
        ApiResponse apiResponse;
        try {
            apiResponse = apiService.userDetailsApi().updateUser(params[0]);
            return apiResponse;
        } catch (Exception e) {
            return new ApiResponse(ApiResponseCode.ERROR, e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(ApiResponse apiResponse) {
        broadcastManager.post(apiResponse);
    }
}
