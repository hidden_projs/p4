package uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.data;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import uk.co.newlevelhealth.myosca.androidapp.R;

public class HomeDataFragment extends Fragment implements IHomeDataContract.View {

    @Override
    public void onAttach(Activity activity) {
        inject();
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home_fr_data, container, false);
        bindView(view);
        return view;
    }

    @Override
    public void inject() {
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void bindView(View view) {
        ButterKnife.bind(this, view);
    }
}
