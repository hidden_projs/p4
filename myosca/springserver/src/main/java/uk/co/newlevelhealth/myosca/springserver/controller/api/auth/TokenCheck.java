package uk.co.newlevelhealth.myosca.springserver.controller.api.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;

@RestController
public class TokenCheck {

    @Autowired
    ObjectMapper mapper;

    @RequestMapping(value = "/api/user/token_check", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String token_check() throws JsonProcessingException {
        ApiResponse apiResponse = new ApiResponse(ApiResponseCode.OK, "OK");
        String object = mapper.writeValueAsString(apiResponse);
        return object;
    }
}
