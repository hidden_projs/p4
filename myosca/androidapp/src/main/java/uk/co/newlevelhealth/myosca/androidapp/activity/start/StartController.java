package uk.co.newlevelhealth.myosca.androidapp.activity.start;

import android.content.Intent;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.activity.login.LoginActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.welcome.WelcomeActivity;
import uk.co.newlevelhealth.myosca.androidapp.security.AppSecurityManager;

public class StartController implements IStartContract.Controller {

    private final AppSecurityManager appSecurityManager;

    private final StartActivity activity;

    @Inject
    public StartController(@NonNull final AppSecurityManager appSecurityManager,
                           @NonNull final StartActivity activity) {
        Preconditions.checkNotNull(appSecurityManager);
        Preconditions.checkNotNull(activity);
        this.activity = activity;
        this.appSecurityManager = appSecurityManager;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onCreate() {
        appSecurityManager.logOut();
        Intent intent;
        if (appSecurityManager.isLoggedIn()) {
            intent = new Intent(activity, WelcomeActivity.class);
        } else {
            intent = new Intent(activity, LoginActivity.class);
        }
        activity.startActivity(intent);
        activity.finish();
    }
}