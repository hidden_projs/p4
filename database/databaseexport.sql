CREATE DATABASE  IF NOT EXISTS `myoscadb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `myoscadb`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: myoscadb
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `myosca_user`
--

DROP TABLE IF EXISTS `myosca_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `myosca_user` (
  `email` varchar(45) NOT NULL,
  `password` varchar(45) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myosca_user`
--

LOCK TABLES `myosca_user` WRITE;
/*!40000 ALTER TABLE `myosca_user` DISABLE KEYS */;
INSERT INTO `myosca_user` VALUES ('ad3min@as.ru','admin12',1),('admin','admin12',1),('admin2@as.ru','admin12',1),('admin@as.ru','admin12',1),('wadmin@as.ru','admin12',1);
/*!40000 ALTER TABLE `myosca_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `myosca_user_details`
--

DROP TABLE IF EXISTS `myosca_user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `myosca_user_details` (
  `email` varchar(45) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`email`),
  CONSTRAINT `fk_user_details_user` FOREIGN KEY (`email`) REFERENCES `myosca_user` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myosca_user_details`
--

LOCK TABLES `myosca_user_details` WRITE;
/*!40000 ALTER TABLE `myosca_user_details` DISABLE KEYS */;
INSERT INTO `myosca_user_details` VALUES ('admin2@as.ru','TestF','TestL','1989-12-31','male',76),('admin@as.ru','ljljlj','yftno','2008-12-31','male',1);
/*!40000 ALTER TABLE `myosca_user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `myosca_user_role`
--

DROP TABLE IF EXISTS `myosca_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `myosca_user_role` (
  `myosca_user_roles_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`myosca_user_roles_id`),
  KEY `fk_user_roles_user_idx` (`email`),
  CONSTRAINT `fk_user_roles_user` FOREIGN KEY (`email`) REFERENCES `myosca_user` (`email`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `myosca_user_role`
--

LOCK TABLES `myosca_user_role` WRITE;
/*!40000 ALTER TABLE `myosca_user_role` DISABLE KEYS */;
INSERT INTO `myosca_user_role` VALUES (1,'admin','ADMIN'),(2,'admin@as.ru','ADMIN');
/*!40000 ALTER TABLE `myosca_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'myoscadb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-16 21:18:50
