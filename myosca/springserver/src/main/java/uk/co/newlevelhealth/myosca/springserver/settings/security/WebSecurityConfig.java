package uk.co.newlevelhealth.myosca.springserver.settings.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import uk.co.newlevelhealth.myosca.springserver.settings.security.jwt.JWTAuthenticationFilter;
import uk.co.newlevelhealth.myosca.springserver.settings.security.jwt.JWTLoginFilter;
import uk.co.newlevelhealth.myosca.springserver.settings.security.jwt.TokenAuthenticationService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DriverManagerDataSource dataSource;

    @Bean
    public TokenAuthenticationService tokenAuthenticationService(){
        return new TokenAuthenticationService();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // disable caching
        http.headers().cacheControl();
        http.exceptionHandling().accessDeniedPage("/403");
        // @formatter:off
        http
            .csrf().disable() // disable csrf for our requests.
            .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/bower_components/**", "/app/**", "/assets/**", "index.html", "/app.js", "/favicon.ico","/").permitAll()
            .antMatchers(HttpMethod.POST, "/api/auth/*").permitAll()
            .anyRequest().authenticated()
            // We filter the api/login requests
            .and().addFilterBefore(new JWTLoginFilter("/api/auth/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
            // And filter other requests to check the presence of JWT in header
            .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

        // @formatter:on
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // Create a default account
        auth.jdbcAuthentication().dataSource(dataSource).usersByUsernameQuery(
                "select email, password, active from myosca_user where " + "email=?").authoritiesByUsernameQuery(
                "select email, role from myosca_user_role where email=?");
    }
}
