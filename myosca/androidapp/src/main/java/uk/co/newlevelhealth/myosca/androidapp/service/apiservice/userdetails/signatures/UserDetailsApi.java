package uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.signatures;

import android.support.annotation.NonNull;

import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

public interface UserDetailsApi {
    ApiResponse updateUser(@NonNull User user) throws Exception;
    ApiResponse getUser() throws Exception;
}
