package uk.co.newlevelhealth.myosca.springserver.controller.api.admin.artifacts;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ArtifactsConfig {
    @Bean
    public FilesStructure filesStructure() {
        return new FilesStructure();
    }
}
