package uk.co.newlevelhealth.myosca.androidapp.security;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthHttpApi;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

import static uk.co.newlevelhealth.myosca.androidapp.util.Utils.Constants.BASE_URL;
import static uk.co.newlevelhealth.myosca.androidapp.util.Utils.Constants.MYOSCA_DEBUG_TAG;
import static uk.co.newlevelhealth.myosca.androidapp.util.Utils.Constants.OK_HTTP3_MEDIA_TYPE_JSON;

public class AuthenticationProviderImpl implements AuthenticationProvider {
    private final Gson gson;

    private final Context context;

    private AuthHttpApi authParserImpl;

    @Inject
    public AuthenticationProviderImpl(final @NonNull OkHttpClient okHttpClient,
                                      final @NonNull Gson gson,
                                      final @NonNull Context context) {
        Preconditions.checkNotNull(okHttpClient);
        Preconditions.checkNotNull(gson);
        Preconditions.checkNotNull(context);
        this.gson = gson;
        this.context = context;

        authParserImpl = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(AuthHttpApi.class);
    }

    @Override
    public ApiResponse register(User user) throws Exception {
        if (!Utils.ConnectionChecker.hasConnection(context)) {
            return new ApiResponse(ApiResponseCode.ERROR, "No Internet " +
                    "Connection");
        }
        final String un = user.getUsername();
        final String pw = user.getPassword();
        final RequestBody requestBody = createRegisterRequestBody(un, pw);
        final Call<ApiResponse> call = authParserImpl.register(requestBody);
        try {
            final Response<ApiResponse> response = call.execute();
            Log.d(MYOSCA_DEBUG_TAG, response.toString());
            final ApiResponse apiResponse = response.body();
            return apiResponse;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @Override
    public ApiResponse login(@NonNull User user) throws Exception {
        if (!Utils.ConnectionChecker.hasConnection(context)) {
            return new ApiResponse(ApiResponseCode.ERROR, "No Internet " +
                    "Connection");
        }
        final String un = user.getUsername();
        final String pw = user.getPassword();
        final RequestBody requestBody = createLoginRequestBody(un, pw);
        final Call<Void> call = authParserImpl.login(requestBody);
        try {
            final Response<Void> response = call.execute();
            String token = response.headers().get("Authorization").replace("Bearer ", "");
            return new ApiResponse(1, token);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Invalid User");
        }
    }

    private RequestBody createLoginRequestBody(String un, String pw) {
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Utils.Constants.USERCONST.PARAMETER_USERNAME, un);
            jsonObject.put(Utils.Constants.USERCONST.PARAMETER_PASSWORD, pw);
            final String content = jsonObject.toString();
            return RequestBody.create(OK_HTTP3_MEDIA_TYPE_JSON, content);
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IllegalStateException("Could not create Login Http Post request!");
        }
    }

    private RequestBody createRegisterRequestBody(String un, String pw) {
        User user = new User();
        user.setUsername(un);
        user.setPassword(pw);
        return RequestBody.create(OK_HTTP3_MEDIA_TYPE_JSON, gson.toJson(user));
    }
}
