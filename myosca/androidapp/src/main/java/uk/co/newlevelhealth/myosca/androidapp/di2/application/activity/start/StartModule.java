package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.start;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.start.IStartContract;
import uk.co.newlevelhealth.myosca.androidapp.activity.start.StartActivity;

@Module
public abstract class StartModule {
    @Binds
    public abstract IStartContract.View bindStartView(StartActivity startActivity);
}
