package uk.co.newlevelhealth.myosca.springserver.dao.user;

import org.springframework.data.repository.CrudRepository;

public interface MyOSCAUserRepository extends CrudRepository<MyOSCAUserEntity, String> {

}