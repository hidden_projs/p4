package uk.co.newlevelhealth.myosca.androidapp.activity.login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.HomeActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.RegisterActivity;
import uk.co.newlevelhealth.myosca.androidapp.security.AppSecurityManager;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;
import uk.co.newlevelhealth.myosca.androidapp.security.task.AuthTask;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

public class LoginController implements ILoginContract.Controller {
    private final ILoginContract.View view;
    private final LoginActivity activity;
    private final BroadcastManager broadcastManager;
    private final Context context;
    private final AuthenticationProvider authenticationProvider;
    private final AppSecurityManager appSecurityManager;

    @Inject
    public LoginController(@NonNull final ILoginContract.View view,
                           @NonNull final LoginActivity activity,
                           @NonNull final BroadcastManager broadcastManager,
                           @NonNull final Context context,
                           @NonNull final AuthenticationProvider authenticationProvider,
                           @NonNull final AppSecurityManager appSecurityManager) {
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(authenticationProvider);
        Preconditions.checkNotNull(appSecurityManager);

        this.broadcastManager = broadcastManager;
        this.activity = activity;
        this.view = view;
        this.context = context;
        this.authenticationProvider = authenticationProvider;
        this.appSecurityManager = appSecurityManager;
    }

    public void attemptLogin() {
        if (validateUserInput()) {
            User user = new User();
            user.setUsername(view.getMTxtUN().getText().toString().trim());
            user.setPassword(view.getMTxtPW().getText().toString().trim());
            view.getMBtnLogin().setEnabled(false);
            view.getMBtnLogin().setAlpha(.5f);
            broadcastManager.register(this);
            new AuthTask(authenticationProvider, broadcastManager).execute(user);
        }
    }

    @Subscribe
    public void onEvent(ApiResponse event) {
        broadcastManager.unregister(this);
        view.getMBtnLogin().setEnabled(true);
        view.getMBtnLogin().setAlpha(1f);
        if (event.getStatus() == ApiResponseCode.OK) {
            confirmAuthentication(event.getPayload());
        } else {
            showAuthenticationError(event.getPayload());
        }
    }

    @Override
    public void confirmAuthentication(String token) {
        appSecurityManager.logIn(token);
        Intent intent = new Intent(context, HomeActivity.class);
        activity.startActivity(intent);
        activity.finishActivity(0);
    }

    @Override
    public void showAuthenticationError(String error) {
        Utils.ToastCreator.showToast(activity, error);
    }

    @Override
    public void goToRegister() {
        Intent intent = new Intent(context, RegisterActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public boolean validateUserInput() {
        boolean valid = true;
        String un = view.getMTxtUN().getText().toString().trim();
        String pw = view.getMTxtPW().getText().toString().trim();
        if (un.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(un).matches()) {
            view.getMWrapperUN().setError("Enter a valid email address");
            valid = false;
        } else {
            view.getMWrapperUN().setError(null);
        }
        if (pw.isEmpty() || pw.length() < 4 || pw.length() > 10) {
            view.getMWrapperPW().setError("Between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            view.getMWrapperPW().setError(null);
        }
        return valid;
    }

    @Override
    public void onDestroy() {
        broadcastManager.unregister(this);
    }

    @Override
    public void onCreate() {
    }
}
