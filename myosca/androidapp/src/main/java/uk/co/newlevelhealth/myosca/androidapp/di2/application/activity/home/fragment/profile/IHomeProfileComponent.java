package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.profile;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.profile.HomeProfileFragment;

@Subcomponent(modules = {HomeProfileModule.class})
public interface IHomeProfileComponent extends AndroidInjector<HomeProfileFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<HomeProfileFragment> {
    }
}
