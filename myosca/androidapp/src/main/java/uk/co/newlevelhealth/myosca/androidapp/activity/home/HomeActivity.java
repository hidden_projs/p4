package uk.co.newlevelhealth.myosca.androidapp.activity.home;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasDispatchingSupportFragmentInjector;
import uk.co.newlevelhealth.myosca.androidapp.R;

public class HomeActivity extends AppCompatActivity implements IHomeContract.View, HasDispatchingSupportFragmentInjector {

    @Inject
    HomeController homeController;

    @Inject
    DispatchingAndroidInjector<Fragment> supportFragmentInjector;

    @BindView(R.id.home_fragment_container)
    FrameLayout mFragmentContainer;

    @BindView(R.id.home_bottom_navigation)
    BottomNavigationView mBottomNav;

    @BindView(R.id.home_greeting_img)
    ImageView mGreetingsImg;

    @BindView(R.id.home_greeting_txt)
    TextView mGreetingsTxt;

    @BindView(R.id.home_toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindView();
        homeController.onCreate();
    }

    @Override
    protected void onDestroy() {
        homeController.onDestroy();
        super.onDestroy();
    }

    @Override
    public void inject() {
        AndroidInjection.inject(this);
    }

    @Override
    public void bindView() {
        ButterKnife.bind(this);
    }

    @Override
    public BottomNavigationView getMBottomNav() {
        return mBottomNav;
    }

    @Override
    public FrameLayout getMFragmentContainer() {
        return null;
    }

    @Override
    public ImageView getMGreetingsImg() {
        return mGreetingsImg;
    }

    @Override
    public TextView getMGreetingsTxt() {
        return null;
    }

    @Override
    public Toolbar getMToolbar() {
        return mToolbar;
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return supportFragmentInjector;
    }
}
