package uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature;

import android.view.View;

public interface IButteredKnifeFragment {
    void bindView(View view);
}
