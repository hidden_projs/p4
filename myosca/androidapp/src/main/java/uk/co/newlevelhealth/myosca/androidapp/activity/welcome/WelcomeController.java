package uk.co.newlevelhealth.myosca.androidapp.activity.welcome;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import dagger.internal.Preconditions;

public class WelcomeController implements IWelcomeContract.Controller {
    private final IWelcomeContract.View view;

    private final WelcomeActivity activity;

    private final Context context;

    @Inject
    public WelcomeController(@NonNull final IWelcomeContract.View view,
                             @NonNull final WelcomeActivity activity,
                             @NonNull final Context context) {
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(context);

        this.context = context;
        this.view = view;
        this.activity = activity;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onCreate() {

    }
}
