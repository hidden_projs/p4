package uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.ApiServiceProviderImpl;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;

public class UserGetTask extends AsyncTask<Void, Void, ApiResponse> {

    private final ApiServiceProviderImpl apiService;

    private final BroadcastManager broadcastManager;

    public UserGetTask(@NonNull ApiServiceProviderImpl apiService,
                       @NonNull BroadcastManager broadcastManager) {
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(apiService);
        this.apiService = apiService;
        this.broadcastManager = broadcastManager;
    }

    @Override
    protected ApiResponse doInBackground(Void... params) {
        Log.d(Utils.Constants.MYOSCA_DEBUG_TAG, "Started Get User task");
        ApiResponse apiResponse;
        try {
            apiResponse = apiService.userDetailsApi().getUser();
            return apiResponse;
        } catch (Exception e) {
            return new ApiResponse(ApiResponseCode.ERROR, e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(ApiResponse apiResponse) {
        broadcastManager.post(apiResponse);
    }
}
