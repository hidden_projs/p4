package uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.signatures;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;

public interface UserDetailsApiCalls {
    @PUT("/api/user")
    Call<ApiResponse> update(@Header("Authorization") String contentRange,
                             @Body RequestBody requestBody);

    @GET("/api/user")
    Call<ApiResponse> get(@Header("Authorization") String contentRange);

    @DELETE("/api/user")
    Call<ApiResponse> delete(@Header("Authorization") String contentRange,
                             @Body RequestBody requestBody);
}
