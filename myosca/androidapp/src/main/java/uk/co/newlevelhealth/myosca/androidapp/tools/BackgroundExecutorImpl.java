package uk.co.newlevelhealth.myosca.androidapp.tools;

import android.support.annotation.NonNull;

import org.reactivestreams.Subscription;

import java.util.Collection;

import io.reactivex.functions.Action;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BackgroundExecutor;

//import uk.co.newlevelhealth.myosca.myoscaandroid.di.signatures.IDestroyable;

/**
 * Implementation class for communication in background
 *
 * @see uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BackgroundExecutor
 */
public class BackgroundExecutorImpl implements BackgroundExecutor/*, IDestroyable */ {

    private final Collection<Subscription> subscriptions = null;

    @Override
    public synchronized void execute(@NonNull Action action) {
//        final Subscription subscription = Schedulers.newThread().createWorker().schedule(action);
//        subscriptions.add(subscription);
    }

    @Override
    public synchronized void destroy() {
        for (Subscription subscription : subscriptions) {
            // subscription.unsubscribe();
        }
        subscriptions.clear();
    }
}
