package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home;

import android.support.v4.app.Fragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.HomeActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.IHomeContract;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.data.HomeDataFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.profile.HomeProfileFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.today.HomeTodayFragment;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.data
        .IHomeDataComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.profile
        .IHomeProfileComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.today
        .IHomeTodayComponent;

@Module(subcomponents = {IHomeTodayComponent.class, IHomeProfileComponent.class,
        IHomeDataComponent.class})
public abstract class HomeModule {

    @Binds
    public abstract IHomeContract.View bindHomeView(HomeActivity homeActivity);

    @Binds
    @IntoMap
    @FragmentKey(HomeDataFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment>
    bindHomeDataFragmentInjectorFactory(IHomeDataComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(HomeProfileFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment>
    bindHomeProfileFragmentInjectorFactory(IHomeProfileComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(HomeTodayFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment>
    bindHomeTodayFragmentInjectorFactory(IHomeTodayComponent.Builder builder);
}
