package uk.co.newlevelhealth.myosca.androidapp.activity.register;

import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IController;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IView;

public class IRegisterContract {
    public interface View extends IView {

    }

    public interface Controller extends IController {

    }
}
