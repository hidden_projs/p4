package uk.co.newlevelhealth.myosca.springserver.controller.api.error;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;

@Controller
public class ErrorHandler implements ErrorController {
    private static final String ERROR_PATH = "/error";

    @RequestMapping(value = ERROR_PATH)
    public @ResponseBody
    ResponseEntity<String> handleError() {
        String response = "REST. Most likely 403 Access Denied Error";
        try {
            response = new ObjectMapper().writeValueAsString(new ApiResponse(HttpStatus.FORBIDDEN.value(), response));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(response);
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
