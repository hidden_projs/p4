package uk.co.newlevelhealth.myosca.androidapp.activity.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.R;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.data.HomeDataFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.profile.HomeProfileFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.today.HomeTodayFragment;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;

public class HomeController implements IHomeContract.Controller {

    private final IHomeContract.View view;

    private final HomeActivity activity;

    private final BroadcastManager broadcastManager;

    private final Context context;

    @Inject
    public HomeController(IHomeContract.View view,
                          HomeActivity activity,
                          Context context,
                          BroadcastManager broadcastManager) {
        Preconditions.checkNotNull(activity, "");
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(view);

        this.context = context;
        this.view = view;
        this.activity = activity;
        this.broadcastManager = broadcastManager;
    }

    @Override
    public void onCreate() {
        switch (Utils.getTimeOfDay()) {
            case "morning":
                view.getMGreetingsImg().setImageResource(R.drawable.morning);
                view.getMToolbar().setTitle("Good Morning!");
                break;
            case "afternoon":
                view.getMGreetingsImg().setImageResource(R.drawable.afternoon);
                view.getMToolbar().setTitle("Good Afternoon!");
                break;
            case "evening":
                view.getMGreetingsImg().setImageResource(R.drawable.evening);
                view.getMToolbar().setTitle("Good Evening!");
                break;
            case "night":
                view.getMGreetingsImg().setImageResource(R.drawable.night);
                view.getMToolbar().setTitle("Good Night!");
                break;
        }

        view.getMBottomNav().setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment = null;
                switch (item.getItemId()) {
                    case R.id.bottom_navigation_action_data:
                        fragment = new HomeDataFragment();
                        break;
                    case R.id.bottom_navigation_action_today:
                        fragment = new HomeTodayFragment();
                        break;
                    case R.id.bottom_navigation_action_profile:
                        fragment = new HomeProfileFragment();
                        break;
                }
                final FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.home_fragment_container, fragment).commit();
                return true;
            }
        });

        activity.getSupportFragmentManager().beginTransaction().add(R.id.home_fragment_container, new HomeTodayFragment()).commit();
        view.getMBottomNav().getMenu().getItem(1).setChecked(true);
    }

    @Override
    public void onDestroy() {
        broadcastManager.unregister(this);
    }
}
