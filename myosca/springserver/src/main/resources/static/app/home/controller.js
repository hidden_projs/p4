﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', Controller);

    function Controller($location, AuthenticationService, ApiService) {
        var vm = this;
        vm.logout = logout;
        vm.buildNow = buildNow;
        vm.getAndroidApk = getAndroidApk;
        vm.getServerJar = getServerJar;
        vm.builds = {};

        initController();

        function initController() {
            getBuilds();
        }

        function logout() {
            AuthenticationService.Logout();
        }

        function buildNow() {
            ApiService.triggerBuild(function (isSuccessful) {
                if (isSuccessful) {
                    console.log("Build Started")
                } else {
                    console.log("Build did not start")
                }
            })
        }

        function getBuilds() {
            ApiService.getBuilds(function (data) {
                vm.builds = data;
            });
        }

        function getAndroidApk(version) {
            ApiService.getAndroidApk(version, function (data) {
                vm.builds = data;
            });
        }

        function getServerJar(version) {
            ApiService.getServerJar(version, function (data) {
                vm.builds = data;
            });
        }
    }
})();