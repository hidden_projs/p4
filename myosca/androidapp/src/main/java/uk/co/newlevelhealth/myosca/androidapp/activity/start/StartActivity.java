package uk.co.newlevelhealth.myosca.androidapp.activity.start;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import uk.co.newlevelhealth.myosca.androidapp.R;

public class StartActivity extends AppCompatActivity implements IStartContract.View {
    @Inject
    StartController startController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        inject();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        bindView();
        startController.onCreate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        startController.onDestroy();
    }

    @Override
    public void bindView() {
        ButterKnife.bind(this);
    }

    @Override
    public void inject() {
        AndroidInjection.inject(this);
    }
}
