package uk.co.newlevelhealth.myosca.androidapp.di2.application.module;

import android.support.annotation.NonNull;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import uk.co.newlevelhealth.myosca.androidapp.MyOSCAAndroidApplication;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.MyOSCAAndroidApplicationScope;

/**
 * This module provides:
 * <p/>
 * An HTTP & HTTP/2 client for Android and Java applications.
 * <p/>
 * Fast and easy way to communicate over HTTP. Also provides WebServer for Testing and Debugging.
 */
@Module
public class HttpModule {

    private static final int CACHE_SIZE = 5 * 1024 * 1024;

    private static final int CONNECTION_TIMEOUT = 10;

    private static final int READ_TIMEOUT = 5;

    private static final int WRITE_TIMEOUT = 5;

    private static final TimeUnit TIMEOUT_TIME_UNIT = TimeUnit.SECONDS;

    @Provides
    @MyOSCAAndroidApplicationScope
    public OkHttpClient provideOkHttpClient(@NonNull Cache cache) {
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIMEOUT, TIMEOUT_TIME_UNIT)
                .writeTimeout(WRITE_TIMEOUT, TIMEOUT_TIME_UNIT)
                .readTimeout(READ_TIMEOUT, TIMEOUT_TIME_UNIT)
                .cache(cache)
                .build();
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    public Cache provideCache(@NonNull File cacheFile) {
        return new Cache(cacheFile, CACHE_SIZE);
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    public File provideCacheFile(@NonNull MyOSCAAndroidApplication app) {
        return new File(app.getCacheDir(), "ok3_http");
    }
}