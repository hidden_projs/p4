package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register;

import android.support.v4.app.Fragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.android.support.FragmentKey;
import dagger.multibindings.IntoMap;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.IRegisterContract;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.RegisterActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details
        .RegisterDetailsFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email
        .RegisterEmailFragment;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register.fragment.details
        .IRegisterDetailsComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register.fragment.email
        .IRegisterEmailComponent;

@Module(subcomponents = {IRegisterEmailComponent.class, IRegisterDetailsComponent.class})
public abstract class RegisterModule {
    @Binds
    public abstract IRegisterContract.View bindRegisterView(RegisterActivity activity);

    @Binds
    @IntoMap
    @FragmentKey(RegisterDetailsFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment>
    bindRegisterDetailsFragment(IRegisterDetailsComponent.Builder builder);

    @Binds
    @IntoMap
    @FragmentKey(RegisterEmailFragment.class)
    public abstract AndroidInjector.Factory<? extends Fragment>
    bindRegisterEmailFragment(IRegisterEmailComponent.Builder builder);
}
