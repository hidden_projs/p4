(function () {
    'use strict';

    var app = angular.module('app', ['ui.router', 'ngMessages', 'ngStorage', 'ngFileSaver']);

    app.config(config).factory('authHttpResponseInterceptor', ['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
        return {
            response: function (response) {
                
                if (response.status === 401) {
                    console.log("Response 401");
                } else if (response.status === 403) {
                    console.log("Response 403");
                }
                return response || $q.when(response);
            },
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    console.log("Response Error 401", rejection);
                    $location.path('/login').search('returnTo', $location.path());
                } else if (rejection.status === 403) {
                    $localStorage.$reset();
                    console.log("Response Error 403", rejection);
                }
                return $q.reject(rejection);
            },
            'request': function (config) {
                config.headers = config.headers || {};
                if ($localStorage.token) {
                    config.headers.Authorization = $localStorage.token;
                }
                return config;
            }
        }
    }]);

    app.config(['$httpProvider', function ($httpProvider) {
        //Http Intercpetor to check auth failures for xhr requests
        $httpProvider.interceptors.push('authHttpResponseInterceptor');
    }]);

    app.run(run);

    function config($stateProvider, $urlRouterProvider) {
        // default route
        $urlRouterProvider.otherwise("/");

        // app routes
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/home/view.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'app/login/view.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            });
    }

    function run($rootScope, $http, $location, $localStorage) {
        // keep user logged in after page refresh
        if ($localStorage.currentUser) {
            $http.defaults.headers.common.Authorization = $localStorage.currentUser.token;
        }

        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var publicPages = ['/login'];
            var restrictedPage = publicPages.indexOf($location.path()) === -1;
            if (restrictedPage && !$localStorage.currentUser) {
                $location.path('/login');
            }
        });
    }
})();