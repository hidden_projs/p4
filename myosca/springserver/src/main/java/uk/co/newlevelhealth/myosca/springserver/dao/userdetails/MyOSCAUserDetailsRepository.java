package uk.co.newlevelhealth.myosca.springserver.dao.userdetails;

import org.springframework.data.repository.CrudRepository;

public interface MyOSCAUserDetailsRepository extends CrudRepository<MyOSCAUserDetailsEntity, String> {

}
