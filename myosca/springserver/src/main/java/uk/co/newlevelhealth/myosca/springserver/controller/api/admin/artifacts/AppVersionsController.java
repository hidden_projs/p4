package uk.co.newlevelhealth.myosca.springserver.controller.api.admin.artifacts;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController
public class AppVersionsController {

    public static final String CURRENT_VERSION = "currentversion";

    @Autowired
    FilesStructure filesStructure;
    @Autowired
    ObjectMapper mapper;

    @RequestMapping(value = "/api/admin/app_versions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getVersions() {
        filesStructure.init();
        try {
            String object = mapper.writeValueAsString(filesStructure);
            return object;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "[]";
        }
    }

    @RequestMapping(value = "/api/admin/app_versions/current_version", method = RequestMethod.GET)
    @ResponseBody
    public int getCurrentVersion() {
        filesStructure.init();
        return filesStructure.getCurrentVersion();
    }

    @RequestMapping(value = "/api/admin/app_versions/apk_version/{version}", method = RequestMethod.GET, produces =
            MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public void getApk(@PathVariable("version") Integer version, HttpServletResponse response) throws IOException {

        File file = filesStructure.getVersions().get(version).getApkVersion();
        response.setContentType("application/vnd.android.package-archive");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("File-Name", file.getName());
        response.getOutputStream().write(Files.readAllBytes(file.toPath()));
        response.flushBuffer();
    }

    @RequestMapping(value = "/api/admin/app_versions/server_version/{version}", method = RequestMethod.GET)
    @ResponseBody
    public void getServer(@PathVariable("version") Integer version, HttpServletResponse response) throws IOException {
        File file = filesStructure.getVersions().get(version).getServerVersion();
        response.setContentType("application/java-archive");
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("File-Name", file.getName());
        response.getOutputStream().write(Files.readAllBytes(file.toPath()));
        response.flushBuffer();
    }
}
