package uk.co.newlevelhealth.myosca.springserver.controller.exceptionhandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
@EnableWebMvc
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {
            IllegalArgumentException.class,
            IllegalStateException.class
    })
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public String handleConflict(HttpServletRequest req, RuntimeException ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);

        String response = "REST. Unknown 409 Error";
        try {
            response = new ObjectMapper().writeValueAsString(new ApiResponse(HttpStatus.CONFLICT.value(),
                                                                             "Error 409. " + ex.toString()
            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return response;
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleGeneralError(HttpServletRequest req, Exception ex) {
        log.error("Request: " + req.getRequestURL() + " raised " + ex);
        String response = "REST. Unknown 500 Error";
        try {
            response = new ObjectMapper().writeValueAsString(new ApiResponse(HttpStatus.CONFLICT.value(),
                                                                             "Error 409. " + ex.toString()
            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
                                                                   HttpHeaders headers,
                                                                   HttpStatus status,
                                                                   WebRequest request) {
        String response = "REST. 404 Not Found Error";
        try {
            response = new ObjectMapper().writeValueAsString(new ApiResponse(HttpStatus.NOT_FOUND.value(),
                                                                             "Error 404. " + ex.toString()
            ));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok(response);
    }
}
