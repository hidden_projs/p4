package uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import uk.co.newlevelhealth.myosca.androidapp.R;

public class RegisterDetailsFragment extends Fragment implements IRegisterDetailsContract.View {

    @Inject
    RegisterDetailsController registerDetailsController;

    @BindView(R.id.register_details_txt_first_name)
    AppCompatEditText mTxtFirstName;
    @BindView(R.id.register_details_txt_last_name)
    AppCompatEditText mTxtLastName;
    @BindView(R.id.register_details_txt_dob)
    AppCompatEditText mTxtDOB;
    @BindView(R.id.register_details_txt_gender)
    AppCompatEditText mTxtGender;
    @BindView(R.id.register_details_txt_weight)
    AppCompatEditText mTxtWeight;

    @BindView(R.id.register_details_wrapper_first_name)
    TextInputLayout mWrapperFirstName;
    @BindView(R.id.register_details_wrapper_last_name)
    TextInputLayout mWrapperLastName;
    @BindView(R.id.register_details_wrapper_dob)
    TextInputLayout mWrapperDOB;
    @BindView(R.id.register_details_wrapper_gender)
    TextInputLayout mWrapperGender;
    @BindView(R.id.register_details_wrapper_weight)
    TextInputLayout mWrapperWeight;

    @BindView(R.id.register_details_btn_submit)
    AppCompatButton mBtnRegisterDetails;

    @BindView(R.id.register_details_btn_skip_submit)
    AppCompatButton mBtnSkipRegisterDetails;

    @Override
    public void onAttach(Activity activity) {
        AndroidSupportInjection.inject(this);
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_register_fr_details, container, false);
        bindView(view);
        registerDetailsController.onCreate();
        mTxtFirstName.setText("TestF");
        mTxtLastName.setText("TestL");
        mTxtDOB.setText("1990-01-01");
        mTxtGender.setText("Male");
        mTxtWeight.setText("76");
        return view;
    }

    @Override
    public void inject() {

    }

    @Override
    public void bindView(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public AppCompatEditText getmTxtFirstName() {
        return mTxtFirstName;
    }

    @Override
    public AppCompatEditText getmTxtLastName() {
        return mTxtLastName;
    }

    @Override
    public AppCompatEditText getmTxtDOB() {
        return mTxtDOB;
    }

    @Override
    public AppCompatEditText getmTxtGender() {
        return mTxtGender;
    }

    @Override
    public AppCompatEditText getmTxtWeight() {
        return mTxtWeight;
    }

    @Override
    public TextInputLayout getmWrapperFirstName() {
        return mWrapperFirstName;
    }

    @Override
    public TextInputLayout getmWrapperLastName() {
        return mWrapperLastName;
    }

    @Override
    public TextInputLayout getmWrapperDOB() {
        return mWrapperDOB;
    }

    @Override
    public TextInputLayout getmWrapperGender() {
        return mWrapperGender;
    }

    @Override
    public TextInputLayout getmWrapperWeight() {
        return mWrapperWeight;
    }

    @Override
    public AppCompatButton getmBtnRegisterDetails() {
        return mBtnRegisterDetails;
    }

    @Override
    public AppCompatButton getmBtnSkipRegisterDetails() {
        return mBtnSkipRegisterDetails;
    }

    @OnClick(R.id.register_details_btn_submit)
    void onBtnSubmitClick() {
        registerDetailsController.attemptUpdateDetails();
    }

    @OnClick(R.id.register_details_btn_skip_submit)
    void onBtnDebugSkipSubmitClick() {
        registerDetailsController.debugSkipUpdateDetails();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        registerDetailsController.onDestroy();
    }
}
