﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService', Service);

    function Service($http, $localStorage, $location) {
        var service = {};

        service.Login = Login;
        service.Logout = Logout;

        return service;

        function Login(email, password, callback) {
            $http({
                method: 'POST',
                url: '/api/auth/login',
                data: {username: email, password: password}
            }).then( function successCallback(response) {
                var token = response.headers("Authorization").substring(7);
                // store username and token in local storage to keep user logged in between page refreshes
                $localStorage.currentUser = {email: email, token: token};

                // add jwt token to auth header for all requests made by the $http service
                $http.defaults.headers.common.Authorization = token;

                // execute callback with true to indicate successful login
                callback(true);
            }, function errorCallback() {
                // execute callback with false to indicate failed login
                callback(false);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
        }

        function Logout() {
            // remove user from local storage and clear http auth header
            delete $localStorage.currentUser;
            $http.defaults.headers.common.Authorization = '';
            $location.path('/login');
        }
    }
})();