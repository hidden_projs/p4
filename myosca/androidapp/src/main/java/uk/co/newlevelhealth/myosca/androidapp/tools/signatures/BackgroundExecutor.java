package uk.co.newlevelhealth.myosca.androidapp.tools.signatures;

import io.reactivex.functions.Action;
//import uk.co.newlevelhealth.myosca.myoscaandroid.di.signatures.IDestroyable;

/**
 * Interface to perform actions on separate thread from main ui thread.
 * Substitutes AsyncTask
 */
public interface BackgroundExecutor {
    void execute(Action action);
    void destroy();
}
