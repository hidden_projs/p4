package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.welcome;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.welcome.IWelcomeContract;
import uk.co.newlevelhealth.myosca.androidapp.activity.welcome.WelcomeActivity;

@Module
public abstract class WelcomeModule {
    @Binds
    public abstract IWelcomeContract.View bindWelcomeView(WelcomeActivity welcomeActivity);
}
