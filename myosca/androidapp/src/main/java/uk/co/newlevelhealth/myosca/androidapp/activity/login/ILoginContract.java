package uk.co.newlevelhealth.myosca.androidapp.activity.login;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;

import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IController;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IView;

public interface ILoginContract {
    interface View extends IView {
        AppCompatButton getMBtnLogin();
        TextInputLayout getMWrapperUN();
        TextInputLayout getMWrapperPW();
        AppCompatEditText getMTxtUN();
        AppCompatEditText getMTxtPW();
    }

    interface Controller extends IController {
        boolean validateUserInput();
        void attemptLogin();
        void confirmAuthentication(String token);
        void goToRegister();
        void showAuthenticationError(String error);
    }
}
