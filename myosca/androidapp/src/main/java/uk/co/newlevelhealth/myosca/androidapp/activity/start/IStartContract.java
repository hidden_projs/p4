package uk.co.newlevelhealth.myosca.androidapp.activity.start;

import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IButteredKnifeActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IController;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IDaggerInjected;

public interface IStartContract {
    interface View extends IButteredKnifeActivity, IDaggerInjected {
    }

    interface Controller extends IController {
    }
}
