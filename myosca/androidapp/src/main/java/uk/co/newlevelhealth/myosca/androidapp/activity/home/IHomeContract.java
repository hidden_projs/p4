package uk.co.newlevelhealth.myosca.androidapp.activity.home;

import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IController;
import uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature.IView;

public class IHomeContract {
    public interface View extends IView {
        BottomNavigationView getMBottomNav();
        FrameLayout getMFragmentContainer();
        ImageView getMGreetingsImg();
        TextView getMGreetingsTxt();
        Toolbar getMToolbar();
    }

    public interface Controller extends IController {

    }
}
