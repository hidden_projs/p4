(function () {
    'use strict';

    angular
        .module('app')
        .factory('ApiService', Service);

    function Service($http, FileSaver) {
        var service = {};

        service.getBuilds = getBuilds;
        service.getAndroidApk = getAndroidApk;
        service.getServerJar = getServerJar;
        service.triggerBuild = triggerBuild;

        return service;

        function getBuilds(callback) {
            $http({
                method: 'GET',
                url: '/api/admin/app_versions'
            }).then(function successCallback(response) {
                var temp = angular.fromJson(response.data);
                temp.versionsList = temp.versionsList.reverse();
                callback(temp);
            }, function errorCallback() {
                callback(null);
            });
        }

        function getAndroidApk(version) {
            $http({
                method: 'GET',
                url: '/api/admin/app_versions/apk_version/' + version,
                responseType: "arraybuffer"
            }).then(function successCallback(response) {
                var filename = response.headers('File-Name');
                var contentType = response.headers('Content-Type');
                var blob = new Blob([response.data], {type: contentType});
                FileSaver.saveAs(blob, filename);
            }, function errorCallback(response) {
                console.log('data: ' + response.data);
                console.log('status: ' + response.status);
                console.log('headers: ' + response.headers);
            });
        }

        function getServerJar(version) {
            $http({
                method: 'GET',
                url: '/api/admin/app_versions/server_version/' + version,
                responseType: "arraybuffer"
            }).then(function successCallback(response) {
                var filename = response.headers('File-Name');
                var contentType = response.headers('Content-Type');
                var blob = new Blob([response.data], {type: contentType});
                FileSaver.saveAs(blob, filename);
            }, function errorCallback(response) {
                console.log('data: ' + response.data);
                console.log('status: ' + response.status);
                console.log('headers: ' + response.headers);
                console.log('config: ' + response.config);
            });
        }

        function triggerBuild(callback) {
            $http({
                method: 'GET',
                url: 'https://bbt:62eb0b3a4a1a2252be0afb5e0b30b8b7@projectvm04.cs.st-andrews.ac.uk/job/SHFinal/build?token=yhEFKFc16JetemB3cFc3hBsSIyVQOmENg0qY7FA3'
            }).then(function successCallback() {
                callback(true);
            }, function errorCallback(data, status, headers, config) {
                console.log('data: ' + data);
                console.log('status: ' + status);
                console.log('headers: ' + headers);
                console.log('config: ' + config);
                callback(false);
            });
        }
    }
})();
