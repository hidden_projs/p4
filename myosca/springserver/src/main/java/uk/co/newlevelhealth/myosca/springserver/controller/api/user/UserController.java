package uk.co.newlevelhealth.myosca.springserver.controller.api.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseError;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;
import uk.co.newlevelhealth.myosca.springserver.dao.userdetails.MyOSCAUserDetailsEntity;
import uk.co.newlevelhealth.myosca.springserver.dao.userdetails.MyOSCAUserDetailsRepository;
import uk.co.newlevelhealth.myosca.springserver.settings.security.jwt.TokenAuthenticationService;

import java.sql.Date;

@RestController
public class UserController {
    @Autowired
    MyOSCAUserDetailsRepository myOSCAUserDetailsRepository;

    @Autowired
    ObjectMapper mapper;

    @Autowired
    TokenAuthenticationService tokenAuthenticationService;

    @RequestMapping(value = "/api/user", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody
    String updateUser(@RequestHeader("Authorization") String token, @RequestBody User user) {
        try {
            String username = tokenAuthenticationService.getUsername(token);
            MyOSCAUserDetailsEntity myOSCAUserDetailsEntity = new MyOSCAUserDetailsEntity();
            myOSCAUserDetailsEntity.setEmail(username);
            // yyyy-MM-dd, fails otherwise
            Date dob = Date.valueOf(user.getDob());
            myOSCAUserDetailsEntity.setDob(dob);
            myOSCAUserDetailsEntity.setFirst_name(user.getFirstName());
            myOSCAUserDetailsEntity.setLast_name(user.getLastName());
            myOSCAUserDetailsEntity.setGender(user.getGender());
            myOSCAUserDetailsEntity.setWeight(Integer.valueOf(user.getWeight()));

            myOSCAUserDetailsRepository.save(myOSCAUserDetailsEntity);
            return mapper.writeValueAsString(new ApiResponse(ApiResponseCode.OK, "OK"));
        } catch (Exception e) {
            return String.format(ApiResponseError.VALUE, e.toString());
        }
    }

    @RequestMapping(value = "/api/user", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getUser(@RequestHeader("Authorization") String token) throws Exception {
        try {
            String username = tokenAuthenticationService.getUsername(token);
            if (username == null) return mapper.writeValueAsString(new ApiResponse(ApiResponseCode.ERROR, "Not Found"));

            MyOSCAUserDetailsEntity myOSCAUserDetailsEntity = myOSCAUserDetailsRepository.findOne(username);
            if (myOSCAUserDetailsEntity == null) return mapper.writeValueAsString(new ApiResponse(ApiResponseCode.ERROR,
                                                                                                  "Not Found"
            ));

            User user = new User();
            user.setUsername(username);
            user.setDob(myOSCAUserDetailsEntity.getDob().toString());
            user.setFirstName(myOSCAUserDetailsEntity.getFirst_name());
            user.setLastName(myOSCAUserDetailsEntity.getLast_name());
            user.setWeight(myOSCAUserDetailsEntity.getWeight());
            user.setGender(myOSCAUserDetailsEntity.getGender());

            String payload = mapper.writeValueAsString(user);
            return mapper.writeValueAsString(new ApiResponse(ApiResponseCode.OK, payload));
        } catch (Exception e) {
            return String.format(ApiResponseError.VALUE, e.toString());
        }
    }
}
