package uk.co.newlevelhealth.myosca.androidapp.di2.application.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.MyOSCAAndroidApplicationScope;
import uk.co.newlevelhealth.myosca.androidapp.security.AppSecurityManager;
import uk.co.newlevelhealth.myosca.androidapp.security.AuthenticationProviderImpl;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;

/**
 * This module provides:
 * <p/>
 * Custom Authenticator Service
 */
@Module
public class AuthenticationModule {

    @Provides
    @MyOSCAAndroidApplicationScope
    public AuthenticationProvider provideAuthenticationManager(@NonNull OkHttpClient okHttpClient,
                                                               @NonNull Gson gson,
                                                               @NonNull Context context) {
        return new AuthenticationProviderImpl(okHttpClient, gson, context);
    }

    @Provides
    @MyOSCAAndroidApplicationScope
    public AppSecurityManager appSecurityManager(@NonNull SharedPreferences sharedPreferences) {
        return new AppSecurityManager(sharedPreferences);
    }
}
