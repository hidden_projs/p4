package uk.co.newlevelhealth.myosca.androidapp;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasDispatchingActivityInjector;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.DaggerIApplicationComponent;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.module.ApplicationModule;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;

/**
 * Application class itself.
 * <p/>
 * Instantiation is necessary for "bootstrap injection", so I can use Dependency Graph for Dagger
 */
public class MyOSCAAndroidApplication extends Application implements
        HasDispatchingActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        DaggerIApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build()
                .inject(this);
        Log.d(Utils.Constants.MYOSCA_DEBUG_TAG, "Initialised Application");
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }
}