package uk.co.newlevelhealth.myosca.commonobjects.api;

public class ApiResponseCode {
    public static final int ERROR = -1;
    public static final int OK = 1;
    public static final int UNAUTHORISED = 2;
}
