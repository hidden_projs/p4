package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.profile;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.profile.HomeProfileFragment;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.profile.IHomeProfileContract;

@Module
public abstract class HomeProfileModule {
    @Binds
    public abstract IHomeProfileContract.View bindHomeProfileView(HomeProfileFragment
                                                                          homeProfileFragment);
}
