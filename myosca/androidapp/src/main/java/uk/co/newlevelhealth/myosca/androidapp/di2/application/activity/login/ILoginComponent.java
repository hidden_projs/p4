package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.login;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.login.LoginActivity;

@Subcomponent(modules = LoginModule.class)
public interface ILoginComponent extends AndroidInjector<LoginActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<LoginActivity> {
    }
}