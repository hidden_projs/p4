package uk.co.newlevelhealth.myosca.androidapp.di2.application.module;

import dagger.Module;
import dagger.Provides;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.MyOSCAAndroidApplicationScope;
import uk.co.newlevelhealth.myosca.androidapp.tools.BackgroundExecutorImpl;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BackgroundExecutor;

/**
 * This module provides:
 * <p/>
 * Enhancement for android AsyncTask.
 * <p/>
 * ReactiveX is a combination of the best ideas from the Observer pattern, the Iterator pattern,
 * and functional programming
 * <p/>
 * Easily create event streams or data streams.
 * Compose and transform streams with query-like operators.
 * Subscribe to any observable stream to perform side effects.
 */
@Module
public class BackgroundExecutorModule {

    @Provides
    @MyOSCAAndroidApplicationScope
    BackgroundExecutor provideBackgroundExecutor() {
        return new BackgroundExecutorImpl();
    }
}
