package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register.fragment.email;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email
        .RegisterEmailFragment;

@Subcomponent(modules = {RegisterEmailModule.class})
public interface IRegisterEmailComponent extends AndroidInjector<RegisterEmailFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RegisterEmailFragment> {
    }
}
