# README #

The MyOSCA Application repository.

### What is this repository for? ###

* Personal Oncology Survivorship Care Assistant 
* Get latest version from the Jenkins CI

### How do I get set up? ###

* Project is managed by Gradle build automation system.
* Generate web dependencies
  * cd springserver\src\main\resources\static
  * bower install
* Run gradle :springserver:bootRun
* Set up BASE_URL for android project to point where the server is running
* Build and install apk file to an android device