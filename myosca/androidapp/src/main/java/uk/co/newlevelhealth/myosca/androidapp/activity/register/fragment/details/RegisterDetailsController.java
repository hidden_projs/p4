package uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.DatePicker;

import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.HomeActivity;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.RegisterActivity;
import uk.co.newlevelhealth.myosca.androidapp.security.AppSecurityManager;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.signature.ApiServiceProvider;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.task.UserUpdateTask;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

public class RegisterDetailsController implements IRegisterDetailsContract.Controller {
    private final IRegisterDetailsContract.View view;
    private final RegisterActivity activity;
    private final Context context;
    private final BroadcastManager broadcastManager;
    private final AuthenticationProvider authenticationProvider;
    private final AppSecurityManager appSecurityManager;
    private final Calendar myCalendar = Calendar.getInstance();
    private final ApiServiceProvider apiServiceProvider;

    @Inject
    public RegisterDetailsController(@NonNull final IRegisterDetailsContract.View view,
                                     @NonNull final RegisterActivity activity,
                                     @NonNull final Context context,
                                     @NonNull final BroadcastManager broadcastManager,
                                     @NonNull final AuthenticationProvider authenticationProvider,
                                     @NonNull final AppSecurityManager appSecurityManager,
                                     @NonNull final ApiServiceProvider apiServiceProvider) {
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(authenticationProvider);
        Preconditions.checkNotNull(appSecurityManager);
        Preconditions.checkNotNull(apiServiceProvider);

        this.view = view;
        this.activity = activity;
        this.context = context;
        this.broadcastManager = broadcastManager;
        this.authenticationProvider = authenticationProvider;
        this.appSecurityManager = appSecurityManager;
        this.apiServiceProvider = apiServiceProvider;
    }

    private static boolean isValidDate(String text) {
        if (text == null || !text.matches("\\d{4}-[01]\\d-[0-3]\\d"))
            return false;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setLenient(false);
        try {
            df.parse(text);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }

    @Override
    public void onCreate() {
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        view.getmTxtDOB().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(activity, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        view.getmTxtDOB().setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onDestroy() {
        broadcastManager.unregister(this);
    }

    @Override
    public boolean validateUserInput() {
        boolean valid = true;
        String firstName = view.getmTxtFirstName().getText().toString().trim();
        String lastName = view.getmTxtLastName().getText().toString().trim();
        String dob = view.getmTxtDOB().getText().toString().trim();
        String gender = view.getmTxtGender().getText().toString().trim().toLowerCase();
        String weight = view.getmTxtWeight().getText().toString().trim();
        int weightInt;

        if (firstName.isEmpty()) {
            view.getmWrapperFirstName().setError("Enter a valid first name");
            valid = false;
        } else {
            view.getmWrapperFirstName().setError(null);
        }

        if (lastName.isEmpty()) {
            view.getmWrapperLastName().setError("Enter a valid first name");
            valid = false;
        } else {
            view.getmWrapperLastName().setError(null);
        }

        if (dob.isEmpty() || !isValidDate(dob)) {
            view.getmWrapperDOB().setError("Enter valid DOB (yyyy-mm-dd)");
            valid = false;
        } else {
            view.getmWrapperDOB().setError(null);
        }

        if (!(gender.equals("male") || gender.equals("female") || gender.equals("other"))) {
            view.getmWrapperGender().setError("Enter male|female|other 0" + gender);
            valid = false;
        } else {
            view.getmWrapperGender().setError(null);
        }
        try {
            weightInt = Integer.parseInt(weight);
            if (weightInt < 10 || weightInt > 200) {
                view.getmWrapperWeight().setError("Please enter valid weight");
                valid = false;
            } else {
                view.getmWrapperWeight().setError(null);
            }
        } catch (NumberFormatException nfe) {
            view.getmWrapperWeight().setError("Please enter numeric value");
        }
        return valid;
    }

    @Override
    public void attemptUpdateDetails() {
        if (validateUserInput()) {
            User user = new User();
            user.setFirstName(view.getmTxtFirstName().getText().toString().trim());
            user.setLastName(view.getmTxtLastName().getText().toString().trim());
            user.setDob(view.getmTxtDOB().getText().toString().trim());
            user.setGender(view.getmTxtGender().getText().toString().trim().toLowerCase());
            user.setWeight(Integer.parseInt(view.getmTxtWeight().getText().toString().trim()));

            view.getmBtnRegisterDetails().setEnabled(false);
            view.getmBtnRegisterDetails().setAlpha(.5f);
            broadcastManager.register(this);
            new UserUpdateTask(apiServiceProvider, broadcastManager).execute(user);
        }
    }

    @Subscribe
    public void onEvent(ApiResponse event) {
        broadcastManager.unregister(this);
        view.getmBtnRegisterDetails().setEnabled(true);
        view.getmBtnRegisterDetails().setAlpha(1f);
        if (event.getStatus() != ApiResponseCode.OK) {
            showAttemptUpdateError(event.getPayload() + ". Try later from your profile menu");
            proceedHome();
        } else {
            proceedHome();
        }
    }

    private void proceedHome() {
        Intent intent = new Intent(context, HomeActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public void showAttemptUpdateError(String error) {
        Utils.ToastCreator.showToast(activity, error);
    }

    @Override
    public void debugSkipUpdateDetails() {
        Intent intent = new Intent(context, HomeActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }
}
