package uk.co.newlevelhealth.myosca.springserver.settings.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import uk.co.newlevelhealth.myosca.springserver.controller.exceptionhandler.ExceptionHandlerFilter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public CORSFilter corsFilter() {
        return new CORSFilter();
    }

    @Bean
    public ExceptionHandlerFilter exceptionHandlerFilter() {
        return new ExceptionHandlerFilter();
    }

    @Bean
    public FilterRegistrationBean registerCorsFilter(CORSFilter filter) {
        FilterRegistrationBean reg = new FilterRegistrationBean(filter);
        reg.setOrder(2);
        return reg;
    }

    @Bean
    public FilterRegistrationBean registerExceptionHandlerFilter(ExceptionHandlerFilter filter) {
        FilterRegistrationBean reg = new FilterRegistrationBean(filter);
        reg.setOrder(1);
        return reg;
    }

    @Bean
    public ObjectMapper objectMapperBean() {
        return new ObjectMapper();
    }

    @Bean
    public InternalResourceViewResolver defaultViewResolver() {
        return new InternalResourceViewResolver();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
    }
}
