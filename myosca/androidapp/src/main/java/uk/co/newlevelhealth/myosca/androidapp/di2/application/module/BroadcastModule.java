package uk.co.newlevelhealth.myosca.androidapp.di2.application.module;

import org.greenrobot.eventbus.EventBus;

import dagger.Module;
import dagger.Provides;
import uk.co.newlevelhealth.myosca.androidapp.BuildConfig;
import uk.co.newlevelhealth.myosca.androidapp.di2.application.MyOSCAAndroidApplicationScope;
import uk.co.newlevelhealth.myosca.androidapp.tools.BroadcastManagerImpl;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;

/**
 * This module provides:
 * <p/>
 * Enhancement over Intents
 * Android optimized event bus that simplifies communication between Activities, Fragments,
 * Threads, Services, etc. Less code, better quality. http://greenrobot.org/eventbus/
 * <p/>
 * API describes it enough :)
 */
@Module
public class BroadcastModule {

    @Provides
    @MyOSCAAndroidApplicationScope
    public BroadcastManager provideBroadcastManager() {
        final boolean isDebugBuild = BuildConfig.DEBUG;
        final EventBus eventBus = EventBus.builder()
                .logNoSubscriberMessages(isDebugBuild)
                .logSubscriberExceptions(isDebugBuild)
                .build();
        return new BroadcastManagerImpl(eventBus);
    }
}
