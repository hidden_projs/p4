package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.home.fragment.today;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.home.fragment.today.HomeTodayFragment;

@Subcomponent(modules = {HomeTodayModule.class})
public interface IHomeTodayComponent extends AndroidInjector<HomeTodayFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<HomeTodayFragment> {
    }
}
