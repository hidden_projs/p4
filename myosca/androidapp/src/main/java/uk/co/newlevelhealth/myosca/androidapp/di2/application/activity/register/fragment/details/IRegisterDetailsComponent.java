package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register.fragment.details;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details
        .RegisterDetailsFragment;

@Subcomponent(modules = {RegisterDetailsModule.class})
public interface IRegisterDetailsComponent extends AndroidInjector<RegisterDetailsFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RegisterDetailsFragment> {
    }
}
