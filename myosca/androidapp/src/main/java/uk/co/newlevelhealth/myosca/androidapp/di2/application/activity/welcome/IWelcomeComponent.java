package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.welcome;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.welcome.WelcomeActivity;

@Subcomponent(modules = WelcomeModule.class)
public interface IWelcomeComponent extends AndroidInjector<WelcomeActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<WelcomeActivity> {
    }
}
