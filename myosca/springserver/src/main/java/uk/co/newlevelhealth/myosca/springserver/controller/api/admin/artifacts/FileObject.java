package uk.co.newlevelhealth.myosca.springserver.controller.api.admin.artifacts;

import java.io.File;
import java.util.List;

public class FileObject {
    private File apkVersion;

    private File serverVersion;

    private List<String> changeLog;

    private int version;

    public FileObject(File apkVersion, File serverVersion, List<String> changeLog, int version) {
        this.apkVersion = apkVersion;
        this.serverVersion = serverVersion;
        this.changeLog = changeLog;
        this.version = version;
    }

    public File getApkVersion() {
        return apkVersion;
    }

    public File getServerVersion() {
        return serverVersion;
    }

    public List<String> getChangeLog() {
        return changeLog;
    }

    public int getVersion() {
        return version;
    }
}
