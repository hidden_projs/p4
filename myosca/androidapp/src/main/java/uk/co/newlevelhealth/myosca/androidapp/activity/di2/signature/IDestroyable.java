package uk.co.newlevelhealth.myosca.androidapp.activity.di2.signature;

public interface IDestroyable {
    void onDestroy();
}
