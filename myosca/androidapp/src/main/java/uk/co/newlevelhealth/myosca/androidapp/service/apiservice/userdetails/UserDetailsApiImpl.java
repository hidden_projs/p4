package uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import uk.co.newlevelhealth.myosca.androidapp.security.AppSecurityManager;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.signatures
        .UserDetailsApi;
import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.signatures
        .UserDetailsApiCalls;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

import static uk.co.newlevelhealth.myosca.androidapp.util.Utils.Constants.BASE_URL;
import static uk.co.newlevelhealth.myosca.androidapp.util.Utils.Constants.MYOSCA_DEBUG_TAG;
import static uk.co.newlevelhealth.myosca.androidapp.util.Utils.Constants.OK_HTTP3_MEDIA_TYPE_JSON;

public class UserDetailsApiImpl implements UserDetailsApi {
    private final UserDetailsApiCalls userDetailsApiCallsImpl;

    private final Gson gson;

    private final Context context;

    private final AppSecurityManager appSecurityManager;

    public UserDetailsApiImpl(final @NonNull OkHttpClient okHttpClient,
                              final @NonNull Gson gson,
                              final @NonNull Context context,
                              final @NonNull AppSecurityManager appSecurityManager) {
        Preconditions.checkNotNull(okHttpClient);
        Preconditions.checkNotNull(gson);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(appSecurityManager);

        this.gson = gson;
        this.context = context;
        this.appSecurityManager = appSecurityManager;

        this.userDetailsApiCallsImpl = new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(okHttpClient)
                .build()
                .create(UserDetailsApiCalls.class);
    }

    @Override
    public ApiResponse updateUser(User user) throws Exception {
        if (!Utils.ConnectionChecker.hasConnection(context)) {
            return new ApiResponse(ApiResponseCode.ERROR, "No Internet " +
                    "Connection");
        }

        final Call<ApiResponse> call = userDetailsApiCallsImpl
                .update(appSecurityManager.getToken(), createUpdateRequestBody(user));
        try {
            final Response<ApiResponse> response = call.execute();
            Log.d(MYOSCA_DEBUG_TAG, response.toString());
            final ApiResponse apiResponse = response.body();
            return apiResponse;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    private RequestBody createUpdateRequestBody(User user) {
        return RequestBody.create(OK_HTTP3_MEDIA_TYPE_JSON, gson.toJson(user));
    }

    @Override
    public ApiResponse getUser() throws Exception {
        if (!Utils.ConnectionChecker.hasConnection(context)) {
            return new ApiResponse(ApiResponseCode.ERROR, "No Internet " +
                    "Connection");
        }
        final Call<ApiResponse> call = userDetailsApiCallsImpl.get(appSecurityManager.getToken());
        try {
            final Response<ApiResponse> response = call.execute();
            Log.d(MYOSCA_DEBUG_TAG, response.toString());
            final ApiResponse apiResponse = response.body();
            return apiResponse;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
