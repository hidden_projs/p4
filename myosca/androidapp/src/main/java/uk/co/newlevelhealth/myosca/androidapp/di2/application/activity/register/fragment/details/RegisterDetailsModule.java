package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register.fragment.details;

import dagger.Binds;
import dagger.Module;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details
        .IRegisterDetailsContract;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.details
        .RegisterDetailsFragment;

@Module
public abstract class RegisterDetailsModule {
    @Binds
    public abstract IRegisterDetailsContract.View bindRegisterDetailsView(RegisterDetailsFragment
                                                                                  fragment);
}
