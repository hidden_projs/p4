package uk.co.newlevelhealth.myosca.androidapp.security.task;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;
import uk.co.newlevelhealth.myosca.androidapp.util.Utils;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponse;
import uk.co.newlevelhealth.myosca.commonobjects.api.ApiResponseCode;
import uk.co.newlevelhealth.myosca.commonobjects.pojo.User;

public class AuthTask extends AsyncTask<User, Void, ApiResponse> {

    private final AuthenticationProvider authenticationProvider;

    private final BroadcastManager broadcastManager;

    public AuthTask(@NonNull AuthenticationProvider authenticationProvider,
                    @NonNull BroadcastManager broadcastManager) {
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(authenticationProvider);
        this.authenticationProvider = authenticationProvider;
        this.broadcastManager = broadcastManager;
    }

    @Override
    protected ApiResponse doInBackground(User... params) {
        Log.d(Utils.Constants.MYOSCA_DEBUG_TAG, "Started Login task");
        ApiResponse apiResponse;
        try {
            apiResponse = authenticationProvider.login(params[0]);
            return apiResponse;
        } catch (Exception e) {
            return new ApiResponse(ApiResponseCode.ERROR, e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(ApiResponse apiResponse) {
        broadcastManager.post(apiResponse);
    }
}
