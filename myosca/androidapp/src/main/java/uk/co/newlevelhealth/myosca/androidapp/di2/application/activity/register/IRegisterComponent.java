package uk.co.newlevelhealth.myosca.androidapp.di2.application.activity.register;

import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.RegisterActivity;

@Subcomponent(modules = RegisterModule.class)
public interface IRegisterComponent extends AndroidInjector<RegisterActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<RegisterActivity> {
    }
}