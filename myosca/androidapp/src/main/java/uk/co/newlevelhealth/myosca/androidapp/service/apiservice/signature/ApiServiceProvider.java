package uk.co.newlevelhealth.myosca.androidapp.service.apiservice.signature;

import uk.co.newlevelhealth.myosca.androidapp.service.apiservice.userdetails.signatures
        .UserDetailsApi;

public interface ApiServiceProvider {
    UserDetailsApi userDetailsApi();
}
