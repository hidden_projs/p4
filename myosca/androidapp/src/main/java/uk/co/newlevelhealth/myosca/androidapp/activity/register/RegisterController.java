package uk.co.newlevelhealth.myosca.androidapp.activity.register;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import uk.co.newlevelhealth.myosca.androidapp.R;
import uk.co.newlevelhealth.myosca.androidapp.activity.register.fragment.email
        .RegisterEmailFragment;
import uk.co.newlevelhealth.myosca.androidapp.security.signature.AuthenticationProvider;
import uk.co.newlevelhealth.myosca.androidapp.tools.signatures.BroadcastManager;

public class RegisterController implements IRegisterContract.Controller {

    private IRegisterContract.View view;

    private RegisterActivity activity;

    private BroadcastManager broadcastManager;

    private Context context;

    private AuthenticationProvider authenticationProvider;

    @Inject
    public RegisterController(@NonNull IRegisterContract.View view,
                              @NonNull RegisterActivity activity,
                              @NonNull BroadcastManager broadcastManager,
                              @NonNull Context context,
                              @NonNull AuthenticationProvider authenticationProvider) {
        Preconditions.checkNotNull(activity);
        Preconditions.checkNotNull(broadcastManager);
        Preconditions.checkNotNull(view);
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(authenticationProvider);
        this.broadcastManager = broadcastManager;
        this.activity = activity;
        this.view = view;
        this.context = context;
        this.authenticationProvider = authenticationProvider;
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onCreate() {
        activity.getSupportFragmentManager().beginTransaction()
                .add(R.id.register_fragment_container, new RegisterEmailFragment()).commit();
    }
}
